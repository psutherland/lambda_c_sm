# README file for breaking statistics observations in `lambda_c_summary_SM.mat`

Peter Sutherland - 2020-08-10
- 2024-05-26	- PS	- Added b and integrated SS dissipation.


Further information about the data can be found in the corresponding articles:

The RaDyO 2009 (r09), SoCal 2010 (s10, vi), and HiRes 2010 (h16) data were published in the following articles:
- Sutherland, P. & Melville, W. K. Field measurements and scaling of ocean surface wave-breaking statistics Geophys. Res. Lett., 2013, 40, 3074-3079, http://dx.doi.org/10.1002/grl.50584
- Sutherland, P. & Melville, W. K. Measuring turbulent kinetic energy dissipation at a wavy sea surface J. Atmos. Oceanic Technol., 2015, 32, 1498-1514, http://dx.doi.org/10.1175/JTECH-D-14-00227.1
- Sutherland, P. & Melville, W. K. Field measurements of surface and near-surface turbulence in the presence of breaking waves J. Phys. Oceanogr., 2015, 45, 943-965, http://dx.doi.org/10.1175/JPO-D-14-0133.1

These data have been made available in hopes that they will be useful.  **Works making use of these data should cite the above publications.**


## This data file contains the following variables:
```
  g     = 9.81; % [m/s^2]
  r09   = RaDyO 2009 IR data
  s10   = SoCal 2010 IR data
  vi    = SoCal 2010 visible data 
  h16   = HiRes 2010 (June 2010) IR data 
```

Each of the experiment data files (r09, s10, h16, vi, jk) contains the following fields:
```
         cp: [1×N double]   - Peak wave speed from DWDR [m/s]
         Hs: [1×N double]   - Significant wave height [m]
     u_star: [1×N double]   - Atmospheric friction velocity [m/s]
    waveage: [1×N double]   - c_p/u_* 
        U10: [1×N double]   - 10-m normalized wind speed [m/s]
      rho_a: [1×N double]   - Air density [kg/m^3]
       time: [1×N double]   - matlab time of start of 20-min average period [UTC]
        L09: [100×N double] - Lambda(c) vector for eact averaging time (note that this is L10 and L16 for SoCal and Hires respectively)
        c09: [100×N double] - Crest length speed vector corresponding to L09 [m/s]  (note that this is c10 and c16 for SoCal and Hires respectively)
		  b: [100xN double] - Spectral breaking strength parameter (unavailable for s10 and vi)
   E_ss_int: [1×N double]   - Integrated subsurface TKE dissipation [W/m^2]
   E_lambda: [1×N double]   - Dissipation by breaking [W/m^2] calculated from Phillips 1985 framework (e.g. S&M 2013, Eqn. 3)
```


## Plotting

The Matlab script `plot_LambdaC_SM13_GRL.m` contains routines for loading, bin-averaging, scaling (following Sutherland and Melville, 2013), and plotting the data.



